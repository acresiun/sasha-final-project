## Topics Django:

### Models & ORM
### Static files
### HTML templates
### urls, views (processing our own requests)
### User, register, login
### Sending emails
...
### Bootstrap

## DJANGO create project and app TUTORIAL:

### Legenda:

numele_aplicatiei se inlocuieste cu numele efectiv al aplicatiei(products, users, carts, etc)

numele_proiectului se inlocuieste cu numele efectiv al proiectului (django_shoppy, i_shop, etc)

## Crearea unui proiect Django:
https://docs.djangoproject.com/en/3.2/ref/django-admin/

Deschidem un "cmd", si mergem cu comanda "cd" spre folderul unde creem proiectul.

Deschidem un "cmd", si dam comanda: 
    
    django-admin startproject numele_proiectului

Tot in cmd, dam comanda: 

    cd numele_proiectului

### Crearea unei aplicatii noi in proiectul existent Django:

Deschidem folderul proiectului ca proiect (folderul care il contine pe "manage.py") in PyCharm, si ii creem un nou Virtual Environment.

Restartam terminalul din PyCharm,si dam comanda: pip install django
In terminalul din PyCharm, dam comanda: 

    python manage.py startapp numele_aplicatiei

Ori prima, ori a doua.

Deschidem numele_proiectului/settings.py, si adaugam numele_aplicatiei ca string in lista INSTALLED_APPS pentru a ignora configurarea din apps.py

Deschidem numele_proiectului/settings.py, si adaugam numele_aplicatiei.apps.NumeleAplicatieiConfig ca string in lista INSTALLED_APPS pentru configurarea specifica facuta in apps.py

### Adaugarea URL-urilor unei aplicatii in proiect:

https://docs.djangoproject.com/en/3.2/ref/urls/

https://docs.djangoproject.com/en/3.2/topics/http/urls/

Deschidem folderul aplicatiei, si creem fisierul "urls.py", cu urmatorul continut: 

    urlpatterns = []

Deschidem numele_proiectului/urls.py, si adaugam in lista urlpatterns un nou element: 

    path('numele_aplicatiei/', include('numele_aplicatiei.urls'))

### Crearea unui superuser (inclusiv pentru Django admin panel) 

In terminal, dam comanda: 

    python manage.py createsuperuser

### Pornirea serverului: 

In terminal, dam comanda: 

    python manage.py runserver

## Crearea unei pagini noi (1 view + 1 url + 1 template):

### 1 view:

https://docs.djangoproject.com/en/3.2/topics/http/views/

https://docs.djangoproject.com/en/3.2/topics/class-based-views/

https://docs.djangoproject.com/en/3.2/ref/class-based-views/base/

https://docs.djangoproject.com/en/3.2/ref/views/

https://docs.djangoproject.com/en/3.2/topics/class-based-views/generic-display/

Deschidem fisierul views.py din folderul numele_aplicatiei

Creem o functie noua:

    def pagina_noua_view(request):
        return render(request, "page.html", {})




### 1 template:

In folderul principal creem subfolderul templates.

Creem fisierul page.html, in care adaugam continutul.



### 1 url:
Deschidem fisierul urls.py din folderul numele_aplicatiei.

Adaugam un nou path un lista urlpatterns:
    
    path('page/', views.pagina_noua_view, name="page"), 



## clase in models.py:

https://docs.djangoproject.com/en/3.2/topics/db/models/

https://docs.djangoproject.com/en/3.2/ref/models/fields/

    class Produs(models.Model):
        titlu = models.CharField(max_length=100) # one line text
        descriere = models.TextField() # multi line text
        pret = models.DecimalField(max_digits=10, decimal_places=2) # float values
        stoc = models.IntegerField() # integer values
        activ = models.BooleanField() # boolean, True/False values
        categorie = models.ForeignKey(Categorie, on_delete=models.CASCADE) # foreign keys, has a relationship
        poza = models.ImageField(upload_to='static/product_images') # images
        atasament = models.FileField(upload_to='static/product_images') # file upload



e-shop = folder care configureaza tot proiectul
asgi / wsgi = deploy
urls = url urile proiectului
products=folderul aplicatiei
migrations=toate modificarile facute in baza de date
init.py= ramane gol forever insa este obligatoriu sa fie prezent
admin=inregistreaza modelele in panoul de administrare
apps=configureaza aplicatia curenta(in cazul de fata products)
context_processors=trimite informatii in toate paginile. Legatura se face in settings.py in constanta templates in lista context_processors
forms=contine toate formularele aplicatiei(clase)
models=toate modellele aplicatiei(clase)
tests=testele aplicatiei
urls=toate url urile aplicatiei - apelurile la functia path
views=toate viewurile aplicatiei(clase, functii)
static=contine toate fisierele statice(css, html, images etc)
templates- toate fisierele dinamice
mmm.bat = fisier windows - utilitar pentur facut mmm si migration rapid
requirements.txt = dependintele proiectului
