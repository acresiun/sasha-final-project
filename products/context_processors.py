from products.models import Category


def navbar_data_provider(request):
    all_categories = Category.objects.all()
    return {'categories': all_categories}

