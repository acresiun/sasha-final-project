from django.contrib import admin

# Register your models here.
from django.contrib.auth.models import Permission

from products.models import *
# from import_export import resources
from import_export.admin import ImportExportModelAdmin


# class ProductAdmin(ImportExportModelAdmin, admin.ModelAdmin):
class ProductAdmin(ImportExportModelAdmin):
    # resource_class = Product
    list_display = ('name', 'description', 'stock', 'price', 'category', 'image', 'promoted')
    pass

admin.site.register(Permission)
admin.site.register(Product, ProductAdmin)
admin.site.register(Category)
# admin.site.register(Product)
admin.site.register(Cart)
admin.site.register(CartItem)
admin.site.register(AboutUs)
admin.site.register(ClientProfile)

# https://django-import-export.readthedocs.io/en/latest/getting_started.html
# class ProductResource(resources.ModelResource):
#     class Meta:
#         model = Product
