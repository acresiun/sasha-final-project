# Generated by Django 3.2.7 on 2021-09-29 15:10

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('products', '0008_cart_cartitem'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='image',
            field=models.ImageField(default='static/not_available.png', upload_to='static/product_images/'),
        ),
    ]
