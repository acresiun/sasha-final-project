from functools import reduce

from django.contrib.auth.decorators import permission_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.mixins import UserPassesTestMixin, PermissionRequiredMixin
from django.http import HttpResponse, HttpResponseForbidden
from django.shortcuts import render, redirect

# Create your views here.
from django.views.generic import *

from products.forms import *
from products.models import *


def hello(request):
    print(request.GET)
    print(request.POST)
    print(request.META)
    return HttpResponse('Hello, world!')


def home_page_view(request):
    all_categories = Category.objects.all()  # scoatem toate categoriile intr-o variabila
    last_products = Product.objects.all().order_by('-id')[
                    :4]  # scoatem primele 4 categorii ordonate descrescator dupa id.
    carusel_products = Product.objects.filter(promoted=True)
    return render(request, 'pages/homepage.html',
                  {'categories': all_categories,  # daca vrem sa trimitem date spre template se face acest dictionar.
                   'last_products': last_products,
                   'carusel_products': carusel_products})  # cheile dictionarului sunt numele variabilelor in django template language.


class StaffRequiredMixin(UserPassesTestMixin):
    def test_func(self):
        return self.request.user.is_staff


class AdminPageView(StaffRequiredMixin, TemplateView):
    template_name = 'pages/admin_page.html'


class StaffStatusDecorator:
    def __init__(self, func):
        self.func = func

    def __call__(self, *args, **kwargs):
        request = args[0]
        if request.user.is_staff:
            result = self.func(*args, **kwargs)
            return result
        else:
            return HttpResponseForbidden()


@permission_required('products.view_category')
@StaffStatusDecorator
def category_all_view(request):
    all_categories = Category.objects.all()
    return render(request, 'pages/category_all_view.html',
                  {'categories': all_categories})


@permission_required('products.view_product')
@StaffStatusDecorator
def product_all_view(request):
    all_products = Product.objects.all()
    return render(request, 'pages/product_all_view.html',
                  {'products': all_products})


def category_details_view(request, category_id):
    category = Category.objects.get(id=category_id)  # daca vrem sa scoatem fix un obiect, folosim .get
    all_categories = Category.objects.all()
    products = Product.objects.filter(
        category_id=category_id)  # se scot toate produsele categoriei pentru care am cerut detalii
    return render(request, 'pages/category_details.html',
                  {'categories': all_categories, 'category': category, 'products': products})


class ProductDetailView(DetailView):
    template_name = 'pages/product_details.html'
    model = Product
    context_object_name = 'product'


def opencart_view(request):
    open_carts = Cart.objects.filter(user=request.user, status='open')
    if open_carts.count() > 0:
        open_cart = open_carts[0]
    else:
        open_cart = Cart.objects.create(user=request.user, status='open')
    open_cart_items = CartItem.objects.filter(cart=open_cart)
    # list_total_items = []
    # total_cart = 0
    open_cart_items_with_totals = list(
        map(lambda cart_item: {'cart_item': cart_item, 'total': cart_item.quantity * cart_item.product.price},
            open_cart_items))
    total_cart = reduce(lambda first, second: first + second['total'], open_cart_items_with_totals, 0)
    # for open_cart_item in open_cart_items:
    #     item_total = open_cart_item.quantity * open_cart_item.product.price
    #     list_total_items.append(item_total)
    #     total_cart += item_total

    return render(request, 'pages/open_cart.html',
                  {'open_cart_items_with_totals': open_cart_items_with_totals, 'open_cart': open_cart,
                   'total_cart': total_cart})


class CategoryCreateView(PermissionRequiredMixin, StaffRequiredMixin, CreateView):
    permission_required = 'products.add_category'
    model = Category
    form_class = CategoryForm
    template_name = 'pages/category_create.html'
    success_url = '/admin_page'


class CategoryUpdateView(PermissionRequiredMixin, StaffRequiredMixin, UpdateView):
    permission_required = 'products.change_category'
    model = Category
    form_class = CategoryForm
    template_name = 'pages/category_update.html'
    success_url = '/admin_page'


class CategoryDeleteView(PermissionRequiredMixin, StaffRequiredMixin, DeleteView):
    permission_required = 'products.delete_category'
    # primeste un model si incarca un template in care confirmam stergerea respectivului articol
    model = Category
    template_name = 'pages/category_delete.html'
    success_url = '/admin_page'

    def get_context_data(self,
                         **kwargs):  # pt ca delete view nu ne da obiectul in template, trebuie sa o implementam manual
        context: dict = super().get_context_data(
            **kwargs)
        category = self.object  # pe proprietatea object isi tine obiectul curent update, delete si detail view
        context['category'] = category  # pasam categoria pe cheia category
        return context


class ProductCreateView(PermissionRequiredMixin, StaffRequiredMixin, CreateView):
    permission_required = 'products.add_product'
    model = Product
    form_class = ProductForm
    template_name = 'pages/product_create.html'
    success_url = '/admin_page'


class ProductUpdateView(PermissionRequiredMixin, StaffRequiredMixin, UpdateView):
    permission_required = 'products.change_product'
    model = Product
    form_class = ProductForm
    template_name = 'pages/product_update.html'
    success_url = '/admin_page'


class ProductDeleteView(PermissionRequiredMixin, StaffRequiredMixin, DeleteView):
    permission_required = 'products.delete_product'
    # primeste un model si incarca un template in care confirmam stergerea respectivului articol
    model = Product
    template_name = 'pages/product_delete.html'
    success_url = '/admin_page'

    def get_context_data(self,
                         **kwargs):  # pt ca delete view nu ne da obiectul in template, trebuie sa o implementam manual
        context: dict = super().get_context_data(
            **kwargs)
        product = self.object  # pe proprietatea object isi tine obiectul curent update, delete si detail view
        context['product'] = product  # pasam produsul pe cheia product
        return context


def about_us_view(request):
    all_names = AboutUs.objects.all()
    return render(request, 'pages/about_us.html', {'about_us': all_names})


def register_view(request):
    if request.method == 'POST':
        # instantiem cele 2 formulare folosind datele din dictionarul request.POST
        user_form = UserCreationForm(data=request.POST)
        client_profile_form = ClientProfileForm(data=request.POST)
        if user_form.is_valid() and client_profile_form.is_valid():
            user = user_form.save()
            client_profile = client_profile_form.save()
            # salvam profilul de client si facem un update la coloana user si legam de user-ul creat mai sus
            client_profile.user = user
            client_profile.save()
            return redirect('/')
    else:  # method=get
        # instantiem 2 formulare goale si le trimitem catre template
        user_form = UserCreationForm()
        client_profile_form = ClientProfileForm()
    # Pe get, formularele sunt goale. Pe post, formularele sunt cele instantiate mai sus in if(populate).
    return render(request, 'pages/register.html', {'user_form': user_form, 'client_profile_form': client_profile_form})


# 1a2b3c4d.

def search_view(request):
    print(request.GET)
    user_search = request.GET.get('user_search', None)
    products = Product.objects.filter(name__icontains=user_search)
    return render(request, 'pages/search_results.html', {'products': products})
