from django.contrib.auth.models import User
from django.db import models


class Category(models.Model):
    name = models.CharField(max_length=50)
    parent_category = models.ForeignKey('self', on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f'{self.name}'


class Product(models.Model):
    name = models.CharField(max_length=50)
    description = models.TextField()
    stock = models.IntegerField(default=0)
    price = models.DecimalField(max_digits=10, decimal_places=2)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)
    image = models.ImageField(upload_to='static/product_images/', default='static/not_available.png')
    promoted = models.BooleanField(default=False)

    def __str__(self):
        return f'{self.name}'


class Cart(models.Model):  # cosul user-ului
    STATUS_CHOICES = (('open', 'Open'), ('checked_out', 'Checked Out'), (
    'sent', 'Sent Item'))  # o tupla de tuple pentru dropdown. Primul parametru este valoarea din baza de date,
    # al doilea parametru este valoarea afisata utilizatorului.
    user = models.ForeignKey(User, on_delete=models.CASCADE)  # Clasa User este importata din Django
    status = models.CharField(max_length=15,
                              choices=STATUS_CHOICES)  # primind choices= acest CharField devine dropdown in Html.

    def __str__(self):
        return f'{self.status} cart of {self.user}'


class CartItem(models.Model):  # echivalentul unui produs in cos
    product = models.ForeignKey(Product, on_delete=models.CASCADE)  # are un produs
    quantity = models.IntegerField(default=1)  # are o cantitate
    cart = models.ForeignKey(Cart, on_delete=models.CASCADE)  # apartine unui Cart.

    def __str__(self):
        return f'{self.quantity} X {self.product} in {self.cart}'


class AboutUs(models.Model):
    name = models.CharField(max_length=50)
    position = models.CharField(max_length=250)
    personal_info = models.TextField()
    email = models.EmailField(default="nimic@chiarnimic.wow")

    def __str__(self):
        return f'{self.name}'


class ClientProfile(models.Model):
    phone_number = models.CharField(max_length=20)
    shipping_adress = models.TextField()
    user = models.ForeignKey(User, on_delete=models.CASCADE, null=True, blank=True)

    def __str__(self):
        return f'{self.user.username} {self.phone_number}'
