from django.urls import path

from products import views

urlpatterns = [

    path('hello/', views.hello, name='hello'),
    path('', views.home_page_view, name='home'),
    path('category_details/<int:category_id>/', views.category_details_view, name='category_details'),
    path('product_detail/<int:pk>/', views.ProductDetailView.as_view(), name='product_details'),
    path('open_cart/', views.opencart_view, name='open_cart'),
    path('about_us/', views.about_us_view, name='about_us'),
    path('admin_page/', views.AdminPageView.as_view(), name='admin_page'),
    path('category_all_view/', views.category_all_view, name='category_all_view'),
    path('product_all_view/', views.product_all_view, name='product_all_view'),
    path('category_create/', views.CategoryCreateView.as_view(), name='category_create'),
    path('category_update/<int:pk>/', views.CategoryUpdateView.as_view(), name='category_update'),
    path('category_delete/<int:pk>/', views.CategoryDeleteView.as_view(), name='category_delete'),
    path('product_create/', views.ProductCreateView.as_view(), name='product_create'),
    path('product_update/<int:pk>/', views.ProductUpdateView.as_view(), name='product_update'),
    path('product_delete/<int:pk>/', views.ProductDeleteView.as_view(), name='product_delete'),
    path('register/', views.register_view, name='register'),
    path('search/', views.search_view, name='search'),

]
