from django import forms

from products.models import *


class CategoryForm(forms.ModelForm):
    class Meta:
        model = Category
        fields = '__all__'


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = '__all__'

class ClientProfileForm(forms.ModelForm):
    class Meta:
        model = ClientProfile
        fields =  '__all__'
        exclude = ['user']
